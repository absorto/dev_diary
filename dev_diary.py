#! /usr/bin/python3
# -*- coding=utf-8 -*-

import os
import sys
from datetime import date
import calendar

def processEntry(entry, filename, entryDate):
    if entry == 'quit':
        return False
    # If this is a new day, add new header to file
    today = date.today().strftime("%A, %d")
    if entryDate != today:
        header = f'=== {today} ===\n\n'
        handler.write(header)
    entry.strip()
    handler.write(f'+ {entry}\n')
    return True

month = calendar.month_name[int(date.today().strftime("%m"))]
filename = f'{month}_devDiary.md'
handler = open(filename, 'a')

# add new header for day ONLY if it hasn't one yet...
today = date.today().strftime("%A, %d")
#header = f'=== {today} ===\n'
#handler.write(header)

# Listen for entries
entry = None
while entry != 'quit':
    entry = input('Add entry or write \'quit\' to stop the script\n')
    processEntry(entry, handler, today)
else:
    print('Saving file...\n')
    handler.close()

